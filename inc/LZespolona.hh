#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH
 
/*!
 *  Plik zawiera definicje struktury LZesplona oraz zapowiedzi
 *  przeciazen operatorow arytmetycznych dzialajacych na tej 
 *  strukturze.

 * Modeluje pojecie liczby zespolonej
 */


struct  LZespolona {
  double   re;    /*! Pole repezentuje czesc rzeczywista. */
  double   im;    /*! Pole repezentuje czesc urojona. */
};
/*
 * Dalej powinny pojawic sie zapowiedzi definicji przeciazen operatorow
 */


LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2);
LZespolona  operator - (LZespolona  Skl1,  LZespolona  Skl2);
LZespolona  operator * (LZespolona  Skl1,  LZespolona  Skl2);
LZespolona  operator / (LZespolona sprzezona, int);
int modul(LZespolona  Skl1);
LZespolona Sprzezona(LZespolona  Skl2);
int bladznaku(char, char);
int bladoperatora(char, char, char);
LZespolona dziel(LZespolona sprzezona, int );
int dobryznak(char );



#endif
