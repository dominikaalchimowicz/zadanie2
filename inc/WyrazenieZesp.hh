#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH



#include "LZespolona.hh"

/*!
 * Modeluje zbior operatorow arytmetycznych.
 */
enum Operator { Op_Dodaj = '+', Op_Odejmij = '-', Op_Mnoz = '*', Op_Dziel = '/' };



/*
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego
 */
struct WyrazenieZesp {
  LZespolona   Arg1;   // Pierwszy argument wyrazenia arytmetycznego
  Operator     Op;     // Opertor wyrazenia arytmetycznego
  LZespolona   Arg2;   // Drugi argument wyrazenia arytmetycznego
};

struct statystyka{
  int l_dobrych_odp;
  int l_zlych_odp;
  int l_wszystkich;
};

/*
 * Funkcje ponizej nalezy zdefiniowac w module.
 *
 */


void Wyswietl(WyrazenieZesp WyrZ);
LZespolona Oblicz(WyrazenieZesp  WyrZ);
int wczytywanie_zespolonej(LZespolona &WyrZ);
void Wyswietlwynik(LZespolona Wynik);
void Porownanie(LZespolona Wynik, LZespolona Nowa, statystyka &stat);
#endif
