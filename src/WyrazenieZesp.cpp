#include "WyrazenieZesp.hh"
#include <iostream>
#include <cstring>
#include <cassert>
using namespace std;

void Wyswietl(WyrazenieZesp WyrZ)
{
  cout << "Twoj przyklad to: ";
  cout << noshowpos << '(';
  if( WyrZ.Arg1.re != 0)   cout << WyrZ.Arg1.re;
  if( WyrZ.Arg1.im != 0 && WyrZ.Arg1.im != 1 && WyrZ.Arg1.im != -1)   cout << showpos << WyrZ.Arg1.im << "i" ;
  if( WyrZ.Arg1.im == 1) cout << "+i" ;
  if( WyrZ.Arg1.im == -1) cout << "-i" ;
  cout << ")";
  cout << (char)WyrZ.Op;
  cout << noshowpos << '(';
  if( WyrZ.Arg2.re != 0)   cout << WyrZ.Arg2.re;
  if( WyrZ.Arg2.im != 0 && WyrZ.Arg2.im != 1 && WyrZ.Arg2.im != -1)   cout << showpos << WyrZ.Arg2.im << "i" ;
   if( WyrZ.Arg2.im == 1) cout << "+i" ;
   if( WyrZ.Arg2.im == -1) cout << "-i" ;
  cout << ") \n" << endl;
  
}


void Wyswietlwynik(LZespolona Wynik)
{
  cout << "\t" << noshowpos << '(';
  if( Wynik.re != 0)   cout << Wynik.re;
  if( Wynik.im != 0 && Wynik.im != 1 && Wynik.im != -1)   cout << showpos << Wynik.im << "i" ;
   if( Wynik.im == 1) cout << "+i" ;
   if( Wynik.im == -1) cout << "-i" ;
  cout << ") \n" << endl;
}



int wczytywanie_zespolonej(LZespolona &LiczZ)
{
  int x=0,y=0,z=0;
  char a,b;
  cin >> a;
  if (a != '(') return 1;
  cin >> b;
  if(dobryznak(b) != 0) return 1;
  LiczZ.re = 0;
  LiczZ.im = 0;
  
  /* dla (i)*/
  if (b == 'i'){
    LiczZ.im = 1;
    LiczZ.re = 0;
    cin >> a;
    if (a != ')') return 1;
    return 0;
  }
  /*dla (-...) */
  if (b == '-'){
    cin >> a;
    
   if(dobryznak(a) != 0) return 1;
    
    
    while (a == '1' || a == '2' || a == '3' || a == '4' || a == '5' || a == '6' || a == '7' || a == '8' || a == '9' || a == '0')
    { x++;
      while (x!=y){
      LiczZ.re = LiczZ.re*10 + (a -'0');
        y++;}
      cin >> a;
      if(dobryznak(a) != 0) return 1;
      z=1;
    }
      
      /* dla (-xi) */
    if ( a == 'i' && z == 1 )
    {
      LiczZ.im = LiczZ.re;
      LiczZ.re = 0;
      cin >> a;
      if (a != ')') return 1;
      LiczZ.im *= (-1);
      return 0;
    }
    
    /* dla (-i) */
    if (a == 'i' && z != 1)
    {
      LiczZ.im = -1;
      LiczZ.re = 0;
      cin >> a;
      if (a != ')') return 1;
      return 0;}
      
    /* dla (-x) */
    if (a == ')')
    {
      LiczZ.re *= (-1);
      return 0;
    }
    
    /* dla (-x-...) */
    if (a == '-'){
      cin >> a;
      if(dobryznak(a) != 0) return 1;
      /* dla (-x-i) */
      if (a == 'i')
      {
        LiczZ.im = -1;
        LiczZ.re *= (-1);
        cin >> a;
        if (a != ')') return 1;
        return 0;
      }
        
      while (a == '1' || a == '2' || a == '3' || a == '4' || a == '5' || a == '6' || a == '7' || a == '8' || a == '9' || a == '0')
      { x++;
        while (x!=y){
        LiczZ.im = LiczZ.im*10 + (a -'0');
          y++;}
        cin >> a;
        if(dobryznak(a) != 0) return 1;
        z=1;
      }
      LiczZ.im *= (-1);
      LiczZ.re *= (-1);
      
      if (a != 'i') return 1;
      cin >> a;
      if (a != ')')return 1;
      return 0;
    }
    /* dla (-x+...) */
    if (a == '+'){
      cin >> a;
      if(dobryznak(a) != 0) return 1;
        /* dla (-x+i) */
        if (a == 'i')
        {
          LiczZ.im = 1;
          LiczZ.re *= (-1);
          cin >> a;
          if (a != ')') return 1;
          return 0;
        }
          
        while (a == '1' || a == '2' || a == '3' || a == '4' || a == '5' || a == '6' || a == '7' || a == '8' || a == '9' || a == '0')
        { x++;
          while (x!=y){
          LiczZ.im = LiczZ.im*10 + (a -'0');
            y++;}
          cin >> a;
          if(dobryznak(a) != 0) return 1;
          z=1;
        }
        LiczZ.re *= (-1);
        if (a != 'i') return 1;
        cin >> a;
        if (a != ')') return 1;
        return 0;
      }
  }
  
    /*dla (...) */
  a=b;
  
    while (a == '1' || a == '2' || a == '3' || a == '4' || a == '5' || a == '6' || a == '7' || a == '8' || a == '9' || a == '0')
    { x++;
      while (x!=y){
      LiczZ.re = LiczZ.re*10 + (a -'0');
        y++;}
      cin >> a;
      if(dobryznak(a) != 0) return 1;
      z=1;
    }
      
      /* dla (xi) */
    if ( a == 'i' && z == 1 )
    {
      LiczZ.im = LiczZ.re;
      LiczZ.re = 0;
      cin >> a;
     if (a != ')') return 1;
      return 0;
    }
    
    /* dla (i) */
    if (a == 'i' && z != 1)
    {
      LiczZ.im = 1;
      LiczZ.re = 0;
      cin >> a;
     if (a != ')') return 1;
      return 0;}
      
    /* dla (x) */
    if (a == ')')
    {
      return 0;
    }
    
    /* dla (x-...) */
    if (a == '-'){
      cin >> a;
      if(dobryznak(a) != 0)  return 1;
      /* dla (x-i) */
      if (a == 'i')
      {
        LiczZ.im = -1;
        cin >> a;
        if (a != ')') return 1;
        return 0;
      }
        
      while (a == '1' || a == '2' || a == '3' || a == '4' || a == '5' || a == '6' || a == '7' || a == '8' || a == '9' || a == '0')
      { x++;
        while (x!=y){
        LiczZ.im = LiczZ.im*10 + (a -'0');
          y++;}
        cin >> a;
        if(dobryznak(a) != 0) return 1;
        z=1;
      }
      LiczZ.im *= (-1);
      if (a != 'i') return 1;
      cin >> a;
      if (a != ')') return 1;
      return 0;
    }
    /* dla (x+...) */
    if (a == '+'){
      cin >> a;
      if(dobryznak(a) != 0) return 1;
        /* dla (x+i) */
        if (a == 'i')
        {
          LiczZ.im = 1;
          cin >> a;
          if (a != ')') return 1;
          return 0;
        }
          
        while (a == '1' || a == '2' || a == '3' || a == '4' || a == '5' || a == '6' || a == '7' || a == '8' || a == '9' || a == '0')
        { x++;
          while (x!=y){
          LiczZ.im = LiczZ.im*10 + (a -'0');
            y++;}
          cin >> a;
          if(dobryznak(a) != 0) return 1;
          z=1;
        }
        if (a != 'i') return 1;
        cin >> a;
        if (a != ')') return 1; 
        return 0;
      }
  return 0;
    }


void Porownanie(LZespolona Wynik, LZespolona Nowa, statystyka &stat)
{
  if (Wynik.re == Nowa.re && Wynik.im == Nowa.im){
    cout << "Twoj wynik jest poprawny! :) \n" << endl;
    stat.l_dobrych_odp ++;
  }
  else {
    stat.l_zlych_odp ++;
    cout << "Twoj wynik nie jest poprawny :( \n Poprawny wynik to:" << endl;
    Wyswietlwynik(Wynik);
  }
  stat.l_wszystkich ++;
}

