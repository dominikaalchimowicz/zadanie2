#include "LZespolona.hh"
#include <math.h>
#include <iostream>
using namespace std;
/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;
  
  return Wynik;
}

LZespolona  operator - (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re - Skl2.re;
  Wynik.im = Skl1.im - Skl2.im;
  
  return Wynik;
}

LZespolona  operator * (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;
  LZespolona  tmp1;
  LZespolona  tmp2;
  tmp1.re = Skl1.re * Skl2.re;
  tmp2.re = Skl1.im * Skl2.im;
  Wynik.re = tmp1.re - tmp2.re;
  tmp1.im = Skl1.re * Skl2.im;
  tmp2.im = Skl1.im * Skl2.re;
  Wynik.im = tmp1.im + tmp2.im;
  
  return Wynik;
}

LZespolona Sprzezona(LZespolona tmp1)
{
  LZespolona Wynik;
  
  Wynik.re = tmp1.re;
  Wynik.im = (-1)*tmp1.im;
  
  return Wynik;
  
}

int modul(LZespolona tmp1)
{
 double a, b;
  
  a = tmp1.re * tmp1.re;
  b = tmp1.im * tmp1.im;
  b = a + b;
  
  return b;
  
}

LZespolona  operator / (LZespolona sprzezona,  int modul)
{
  LZespolona Wynik;
  
  Wynik.re = sprzezona.re / modul;
  Wynik.im = sprzezona.im / modul;
  
  return Wynik;
  
}

LZespolona dziel(LZespolona sprzezona, int modul)
{
  LZespolona Wynik;
  
  Wynik.re = sprzezona.re / modul;
  Wynik.im = sprzezona.im / modul;
  
  return Wynik;
  
}

int dobryznak(char a)
{
  while ( a != '-' && a != '+' && a != 'i' && a != ')' && a != '1' && a != '2' && a != '3' && a != '4' && a != '5' && a != '6' && a != '7' && a != '8' && a != '9' && a != '0' && a != '(')
  {
    cin.clear( );
    return 1;
  }
  return 0;
}


